package me.Roervig2473.ArgumentParser;

import java.util.HashMap;

/**
 * 
 * @author ImThatPedoBear
 *
 */
public class Parser
{

	private String[] rawArguments;
	private boolean debug = false;
	private String argumentPrefix;
	private boolean overrideKeys = false;
	private HashMap<String, String> arguments = new HashMap<String, String>();
	
	public Parser(String[] rawArguments)
	{
		
		this.setRawArguments(rawArguments);
		
	}

	public String[] getRawArguments()
	{
	
		return rawArguments;
	
	}

	public void setRawArguments(String[] rawArguments) 
	{
	
		this.rawArguments = rawArguments;
	
	}

	public boolean isDebug() 
	{
		
		return debug;

	}

	public void setDebug(boolean debug) 
	{
	
		this.debug = debug;
	
	}

	public String getArgumentPrefix()
	{
	
		return argumentPrefix;
	
	}

	public void setArgumentPrefix(String argumentPrefix) 
	{
		
		this.argumentPrefix = argumentPrefix;
	
	}

	public boolean isOverrideKeys() 
	{
	
		return overrideKeys;
	
	}

	public void setOverrideKeys(boolean overrideKeys) 
	{
	
		this.overrideKeys = overrideKeys;
	
	}

	public HashMap<String, String> getArguments()
	{
	
		return arguments;
	
	}

	public void setArguments(HashMap<String, String> arguments) 
	{
		
		this.arguments = arguments;
	
	}
	
}